-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Agu 2020 pada 16.04
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_otobenk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 1, 'otobenk', 1, 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `main_kendaraan_tipe`
--

CREATE TABLE `main_kendaraan_tipe` (
  `id` int(11) NOT NULL,
  `nama_tipe` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `main_kendaraan_tipe`
--

INSERT INTO `main_kendaraan_tipe` (`id`, `nama_tipe`) VALUES
(1, 'Matic 110 - 125 CC'),
(2, 'Matic 150 - 155 CC'),
(3, 'Bebek 110 - 125 CC'),
(4, 'Bebek 135 -  150 CC'),
(5, 'Sport 150 CC'),
(6, 'Sport 250 CC');

-- --------------------------------------------------------

--
-- Struktur dari tabel `main_servis`
--

CREATE TABLE `main_servis` (
  `id` int(11) NOT NULL,
  `nama_servis` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `main_servis`
--

INSERT INTO `main_servis` (`id`, `nama_servis`) VALUES
(1, 'Servis Dirumah'),
(2, 'Darurat'),
(3, 'Servis Dibengkel');

-- --------------------------------------------------------

--
-- Struktur dari tabel `main_sub_servis`
--

CREATE TABLE `main_sub_servis` (
  `id` int(11) NOT NULL,
  `id_main_servis` int(11) NOT NULL,
  `nama_sub_servis` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `main_sub_servis`
--

INSERT INTO `main_sub_servis` (`id`, `id_main_servis`, `nama_sub_servis`) VALUES
(1, 1, 'Servis Ringan'),
(2, 1, 'Servis Lengkap'),
(3, 1, 'Tune Up');

-- --------------------------------------------------------

--
-- Struktur dari tabel `main_sub_servis_paket`
--

CREATE TABLE `main_sub_servis_paket` (
  `id` int(11) NOT NULL,
  `id_main_sub_servis` int(11) NOT NULL,
  `nama_item_servis` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `main_sub_servis_paket`
--

INSERT INTO `main_sub_servis_paket` (`id`, `id_main_sub_servis`, `nama_item_servis`) VALUES
(1, 1, 'Penggantian Oli Mesin'),
(2, 1, 'Penggantian Oli Gardan'),
(3, 1, 'Pengecekan lampu dan sistem kelistrikan'),
(4, 1, 'pengecekan dan penyetelan rem depan dan belakang'),
(5, 1, 'Pemeriksaan dan pengencangan mur dan baut'),
(6, 1, 'Pemeriksaan/penyetelan dan pelumasan rantai'),
(7, 1, 'Pemeriksaan dan penambahan air accu'),
(8, 2, 'Penggantian oli mesin'),
(9, 2, 'Penggantian oli gardan'),
(10, 2, 'Pemeriksaan lampu dan sistem kelistrikan'),
(11, 2, 'Pengecekan dan penyetelan rem depan dan belakang'),
(12, 2, 'Pemeriksaan dan pengencangan mur dan baut'),
(13, 2, 'Pemeriksaan/penyetelan dan pelumasan rantai'),
(14, 2, 'Pemeriksaan dan penambahan air accu'),
(15, 2, 'Pemeriksaan dan pembersihan busi'),
(16, 2, 'Pemeriksaan dan pembersihan sistem injeksi'),
(17, 2, 'pemeriksaan dan pembersihan sistem karburator'),
(18, 2, 'pemeriksaan dan pembersihan throttle body'),
(19, 2, 'Pembersihan/penggantian filter oli'),
(20, 2, 'Pemeriksaan dan penambahan cairan radiator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `main_sub_servis_paket_harga`
--

CREATE TABLE `main_sub_servis_paket_harga` (
  `id` int(11) NOT NULL,
  `id_main_sub_servis_paket` int(11) NOT NULL,
  `id_main_kendaraan_tipe` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `main_sub_servis_paket_harga`
--

INSERT INTO `main_sub_servis_paket_harga` (`id`, `id_main_sub_servis_paket`, `id_main_kendaraan_tipe`, `harga`) VALUES
(1, 1, 1, 5000),
(2, 2, 1, 5000),
(3, 3, 1, 15000),
(4, 4, 1, 20000),
(5, 5, 1, 5000),
(6, 6, 1, 10000),
(7, 7, 1, 10000),
(8, 8, 1, 5000),
(9, 9, 1, 5000),
(10, 10, 1, 10000),
(11, 11, 1, 20000),
(12, 12, 1, 5000),
(13, 13, 1, 10000),
(14, 14, 1, 10000),
(15, 15, 1, 15000),
(16, 16, 1, 50000),
(17, 17, 1, 45000),
(18, 18, 1, 70000),
(19, 19, 1, 15000),
(20, 20, 1, 15000),
(21, 20, 2, 123456789);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mitra`
--

CREATE TABLE `mitra` (
  `id` int(11) NOT NULL,
  `id_mitra_owner` int(11) NOT NULL,
  `user_bengkel` varchar(40) NOT NULL,
  `nama_bengkel` varchar(40) NOT NULL,
  `alamat` text NOT NULL,
  `tipe` enum('Roda 2','Roda 4') NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lon` varchar(100) NOT NULL,
  `foto_bengkel` blob NOT NULL,
  `jam_buka` time NOT NULL,
  `jam_tutup` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `mitra`
--

INSERT INTO `mitra` (`id`, `id_mitra_owner`, `user_bengkel`, `nama_bengkel`, `alamat`, `tipe`, `lat`, `lon`, `foto_bengkel`, `jam_buka`, `jam_tutup`) VALUES
(1, 1, 'bengkel_sabu', 'Bengkel Sabu', 'Jl. Sultan Hasanuddin No.46 Balikpapan Barat', 'Roda 4', '-1.223360', '116.814281', '', '07:45:00', '16:45:00'),
(2, 2, 'bengkel_berkah', 'Bengkel Berkah', 'Jl. Sultan Hasanuddin No.47 Balikpapan Barat', 'Roda 2', '-1.224358', '116.813723', '', '07:45:00', '15:00:00'),
(3, 3, 'bengkel_udin', 'Bengkel Udin', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.220689', '116.812082', '', '07:45:00', '15:00:00'),
(4, 4, 'sujarwo_bengkel', 'Bengkel Sujarwo', 'Jl. Sultan Hasanuddin No.49 Balikpapan Barat', 'Roda 2', '-1.220851', '116.812727', '', '08:00:00', '17:00:00'),
(5, 5, 'budi_bengkel', 'Bengkel Mas Budi', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.223919', '116.810229', '', '10:00:00', '19:00:00'),
(6, 6, 'dawet_bengkel', 'Bengkel Kang Dawet', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.225432', '116.811097', '', '09:00:00', '19:00:00'),
(7, 7, 'didi_bengkel', 'Bengkel Bang didi', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.236265', '116.826696', '', '06:00:00', '19:00:00'),
(8, 8, 'suya_bengkel', 'Bengkel Surya', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '1.241843', '116.830695', '', '09:00:00', '18:00:00'),
(9, 9, 'liubei_bengkel', 'Bengkel Liu Bei', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.241253', '116.832936', '', '06:00:00', '18:00:00'),
(10, 10, 'guanpei_bengkel', 'Bengkel Guan Pei', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.241285', '116.833772', '', '07:00:00', '18:00:00'),
(11, 11, 'lingling_bengkel', 'Bengkel Ling Ling', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.246992', '116.835010', '', '07:00:00', '19:00:00'),
(12, 12, 'ipman_bengkel', 'Bengkel IpMan', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.247635', '116.834871', '', '09:00:00', '21:00:00'),
(13, 13, 'brucelee_bengkel', 'Bengkel Brucelee', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.259252', '116.839795', '', '09:00:00', '19:00:00'),
(14, 14, 'mamat_bengkel', 'Bengkel Kang Mamat', 'Jl. Sultan Hasanuddin No.48 Balikpapan Barat', 'Roda 2', '-1.261826', '116.844599', '', '08:00:00', '18:00:00'),
(15, 15, '', '', '', 'Roda 2', '', '', '', '00:00:00', '00:00:00'),
(16, 16, '', '', '', 'Roda 2', '', '', '', '00:00:00', '00:00:00'),
(17, 17, '', '', '', 'Roda 2', '', '', '', '00:00:00', '00:00:00'),
(18, 18, '', '', '', 'Roda 2', '', '', '', '00:00:00', '00:00:00'),
(19, 19, '', '', '', 'Roda 2', '', '', '', '00:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mitra_owner`
--

CREATE TABLE `mitra_owner` (
  `id` int(11) NOT NULL,
  `id_user` varchar(34) NOT NULL,
  `nama_lengkap` varchar(40) NOT NULL,
  `jk` enum('tidak ada','pria','wanita') NOT NULL,
  `no_hp` varchar(14) NOT NULL,
  `no_wa` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `mitra_owner`
--

INSERT INTO `mitra_owner` (`id`, `id_user`, `nama_lengkap`, `jk`, `no_hp`, `no_wa`) VALUES
(1, '5ea3a170a74c94619da7417db3cbb04f', 'Ivan Adinata', 'pria', '08993006200', '08993006200'),
(2, 'cd6d57d43e2650746297324b74267ab8', 'Kucing Betina', 'wanita', '02147371823', '02147371823'),
(3, 'b42139fc86c1f505ae63e96d40fe269d', 'Mitra User', 'pria', '02184783191', '02184783191'),
(4, '4d0f18c1ad068a276e7c3408469129bd', 'Udin Sujarwo', 'pria', '0823494721', '0843923847'),
(5, '694e5bcc378720a9782cd34278816902', 'Mas Budi', 'pria', '089234862', '083742934'),
(6, 'd348f8d70e579c5a0a6995faffed3d54', 'Kang Dawet', 'pria', '0891347423', '0848239439'),
(7, 'b3335082ca14cebf2b5e660bfe8c3cb0', 'Bang Didi', 'pria', '084929342834', '084932848234'),
(8, '57a0d04462acfca9395f939099ecc195', 'Surya', 'pria', '0834728344', '0892347743'),
(9, '3b5096b62deb0d9532c37728e402dc16', 'Liu Bei', 'pria', '08374628348', '08273474832'),
(10, '8ca0a48df1c16bf7b0b879aea432291e', 'Guan Pei', 'pria', '09792834743', '08728347382'),
(11, '119ed127572491481b242204c3d0b1c2', 'Ling Ling', 'wanita', '088934772384', '089238474332'),
(12, '67bf3aa0a22bb0561dddcc53b4a774b3', 'IpMan', 'pria', '0889348232', '0882934843'),
(13, 'e37dd6d2d01ac15d8f0ab0b0199c9b3b', 'Bruce Lee', 'pria', '097374238', '087347382'),
(14, '0d28e22e6d75e5a0a40465e4a5446cb2', 'Kang Mamat', 'pria', '08934729349', '08823949439'),
(15, '232596962c12ba7146e1132d0c6ebbcd', 'Ivan Adinata', 'tidak ada', '', ''),
(16, '5c6a901071fc87259139658d85d0d3c9', 'Yuanita Amara Kasih', 'tidak ada', '', ''),
(17, 'cfd6d8fbbae2077b5931d5de8e8392f9', 'Amel Aulia riski', 'tidak ada', '', ''),
(18, 'eff42e50eb31550d5efa97ea4002d2a4', 'A dan B', 'tidak ada', '', ''),
(19, 'b0c2e19194e6737dca524f932ab24462', 'Warung Bengkel', 'tidak ada', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mitra_service`
--

CREATE TABLE `mitra_service` (
  `id` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `nama_service` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mitra_sub_service`
--

CREATE TABLE `mitra_sub_service` (
  `id` int(11) NOT NULL,
  `nama_sub_service` varchar(100) NOT NULL,
  `id_mitra_service` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mitra_viewer`
--

CREATE TABLE `mitra_viewer` (
  `id` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `promo_slider`
--

CREATE TABLE `promo_slider` (
  `id` int(11) NOT NULL,
  `gambar` text NOT NULL,
  `no_urut` int(11) NOT NULL,
  `date_create` int(11) NOT NULL,
  `status` enum('show','hide') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` varchar(34) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `level` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `verified` int(1) NOT NULL,
  `kode_verified` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `level`, `date_created`, `verified`, `kode_verified`) VALUES
('0d28e22e6d75e5a0a40465e4a5446cb2', 'kangmamat@gmail.com', '$2y$10$BffmJ8TujC2RVlQxcdBXrORM9vVSXmhFiBrgtJc4C88YrQwTJ2uqm', 2, 1580182364, 0, ''),
('119ed127572491481b242204c3d0b1c2', 'lingling@gmail.com', '$2y$10$yf5SDu8ps9f.jKrvGFbl6.Iz1vuLSqlHHTBrV3UqWQAaTAuLS0.D6', 2, 1580182251, 0, ''),
('1a33aab1cb40b06e0b26fb2f5bc5d61c', 'kucing.oren@gmail.com', '$2y$10$zDNEVIrELNREGdLnDGwm/uWmBOEbkqLSCxhLZ/dEifdzwmUIU72zu', 1, 1576503645, 0, 'b7Nja'),
('1d84dea1e725c138ef30275c96e0c9a3', 'ivnadinata@gmail.com', '$2y$10$i8gTuoEvKxV56/Cc3YkbjeuQ76em0qg5HZOBmLLZzuJmLSsZg9CIe', 1, 1576503142, 0, 'Zj7A5'),
('232596962c12ba7146e1132d0c6ebbcd', 'ivanadinata@gmail.com', '$2y$10$A7iG0hujFf5XFPWFAcsRZu.n.gYa6xrpBWK9Jbm6Y.mbeCUiB9AH.', 2, 1583300476, 0, '9e084'),
('3b5096b62deb0d9532c37728e402dc16', 'liubei@gmail.com', '$2y$10$k0Fbv7zHhg47O3bc1LoGbOtYe4qg37xNXssQJpJAKB1SKCQX6wLHK', 2, 1580182220, 0, ''),
('46091cae36b0a569644d65b2000cac19', 'smart.users@gmail.com', '$2y$10$NwH56iEfBneArUhP8a5Yf.jwbf58tnvckEy3v9Ky1IyQGuOGzpbCK', 1, 1576545532, 0, ''),
('4d0f18c1ad068a276e7c3408469129bd', 'udin@gmail.com', '$2y$10$bSM7c617TK6VPK7JzQ70neXszHqD.Lp97G.ySiKpd4gUYigEgj7FO', 2, 1580181994, 0, ''),
('57a0d04462acfca9395f939099ecc195', 'Surya@gmail.com', '$2y$10$Wq66WwvOg5MNEARbwF/rmer4REGAwACpLplfZ.r9AfbNecUFbr6je', 2, 1580182208, 0, ''),
('5b7bc49f44b68c87e41e1eb2a748a318', 'testaja@mail.com', '$2y$10$XXCx4zZgJCX44vokmgyErOA/gd4./tujeGnMcn54OiiI1U85UIPwe', 1, 1576571917, 0, ''),
('5c6a901071fc87259139658d85d0d3c9', 'yuan@gmail.com', '$2y$10$NWAs8eeXv54/L79QNDHE/OFad8YD6GbadIz5OfqiFQf2fTYTbD3gO', 2, 1583365676, 0, '719d5'),
('5ea3a170a74c94619da7417db3cbb04f', 'ivanadinata1111@gmail.com', '$2y$10$l2bNB18AehOIVSKGtn3sTeVFguzLUM6cUd2D0TiPplbAUrHeeUpHy', 2, 1576503294, 0, 'Ane32'),
('67bf3aa0a22bb0561dddcc53b4a774b3', 'ipman@gmail.com', '$2y$10$PbdIURnOG57mb06gwWWViulA721Djv0Q9xXLAW..CQRAbc5c25ty2', 2, 1580182262, 0, ''),
('694e5bcc378720a9782cd34278816902', 'masbudi@gmail.com', '$2y$10$S1b12fJWWKMcOf6IqnNBe.0ZDFADbsAcTJdm93J.ZDJkzevzMXy2y', 2, 1580182121, 0, ''),
('710d773d2164d64210e5dd16dcf69092', 'smart.user@gmail.com', '$2y$10$x3ezDrDPHrMKEQsWDOA6AuJz7FZ2bHzT4L1KvfABt/XgqN96BN.Xy', 1, 1576545072, 0, ''),
('8ca0a48df1c16bf7b0b879aea432291e', 'Guan Pei@gmail.com', '$2y$10$WNc9IIDamHEOJzJ1sCjgSeOs0zv0z0q85n/sVbu3FUZs22YRkGYh2', 2, 1580182236, 0, ''),
('b0c2e19194e6737dca524f932ab24462', 'warungbengkel@gmail.com', '$2y$10$0jTHdUzp7AK1RHaqcn6Z6.Q.9lg6tZwBV8KfXFDkpN/mf/PWcnvnu', 2, 1584453128, 0, 'f50ea'),
('b3335082ca14cebf2b5e660bfe8c3cb0', 'bangdidi@gmail.com', '$2y$10$V3TQLTLyuVOTf2OvawAxK.PukX7o1xvTCFgCVdN3EP/kCbCxgqkYW', 2, 1580182186, 0, ''),
('b42139fc86c1f505ae63e96d40fe269d', 'user@gmail.com', '$2y$10$/v0kp23YqxY5Z3Dmj4PUWO4r9TLko3MUlT1KPl/.oQPQygH1o9B1i', 2, 1576549204, 0, ''),
('b52d99033d1ef4c494b39e62159ceb7e', 'test@mail.com', '$2y$10$eJEUPv6xBIG8YLnr2x491.C9NQu7k..Com0yGNwVrChlOAM7lyR46', 1, 1576569817, 0, ''),
('bad6fa2038f83d62ab80e204b580ff79', 'aa@gmail.co.id', '$2y$10$uGUADKyBLW7DTelQ3lG0SumoK.H/3f1TPcNdFU/LjjS8V2UyF399K', 1, 1584017019, 0, 'b4b1f'),
('cd6d57d43e2650746297324b74267ab8', 'kucing.betina@gmail.com', '$2y$10$fZOYE5zYfODqLbkeECb9ee1R42x/16tBcPRM8qbWGuvVnIY/70rnG', 2, 1576506414, 0, ''),
('cfd6d8fbbae2077b5931d5de8e8392f9', 'amelliarizz@gmail.com', '$2y$10$ZYgqhmy7EyVPkRVZDVsOL.On460GNtZgTKMePCbHtRZernkY6b9rm', 2, 1583410009, 0, '6d28c'),
('d348f8d70e579c5a0a6995faffed3d54', 'kangdawet@gmail.com', '$2y$10$nMxPmeqpMcip3vlYDtJikeV2uRizcFiOsZfOOot8A9LbmXkcpuhqm', 2, 1580182160, 0, ''),
('e37dd6d2d01ac15d8f0ab0b0199c9b3b', 'brucelee@gmail.com', '$2y$10$AalUIh.QJ1MbFX0BRKk7oe9zI82HhZwTePpc3RB9FJsFjXbS.Dlzi', 2, 1580182278, 0, ''),
('eff42e50eb31550d5efa97ea4002d2a4', 'ab@gmail.co.id', '$2y$10$ZrsyF9LlEekl.nN71IMJX.dcIt845D3BNSNfun1D6XspjIgKHGepm', 2, 1584017235, 1, '60bfa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_alamat`
--

CREATE TABLE `user_alamat` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `lat` int(11) NOT NULL,
  `lon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `id_user` varchar(34) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `jk` enum('tidak ada','Pria','Wanita') NOT NULL,
  `no_wa` varchar(14) NOT NULL,
  `no_hp` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_detail`
--

INSERT INTO `user_detail` (`id`, `id_user`, `nama_lengkap`, `jk`, `no_wa`, `no_hp`) VALUES
(1, '1d84dea1e725c138ef30275c96e0c9a3', 'Ivan Adinata', 'Pria', '08993006200', '08993006200'),
(2, '1a33aab1cb40b06e0b26fb2f5bc5d61c', 'Kucing Oren', 'Pria', '', ''),
(4, '710d773d2164d64210e5dd16dcf69092', 'Smart User', 'Pria', '', ''),
(5, '46091cae36b0a569644d65b2000cac19', 'Smart User', 'Pria', '', ''),
(6, 'b52d99033d1ef4c494b39e62159ceb7e', 'Test Mail', 'Pria', '', ''),
(7, '5b7bc49f44b68c87e41e1eb2a748a318', 'Test Aja', 'Pria', '', ''),
(10, 'bad6fa2038f83d62ab80e204b580ff79', 'A dan A', 'tidak ada', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_kendaraan`
--

CREATE TABLE `user_kendaraan` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_kendaraan_merek` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_kendaraan`
--

INSERT INTO `user_kendaraan` (`id`, `id_user`, `id_kendaraan_merek`) VALUES
(1, 3, 11);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_order`
--

CREATE TABLE `user_order` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_main_servis` int(11) NOT NULL,
  `id_main_sub_servis` int(11) NOT NULL,
  `id_mitra` int(11) NOT NULL,
  `id_main_kendaraan_tipe` int(11) NOT NULL,
  `date_create` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user_order`
--

INSERT INTO `user_order` (`id`, `id_user`, `id_main_servis`, `id_main_sub_servis`, `id_mitra`, `id_main_kendaraan_tipe`, `date_create`) VALUES
(1, 1, 1, 1, 1, 1, 155612),
(2, 2, 1, 2, 2, 5, 144312);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_order_detail`
--

CREATE TABLE `user_order_detail` (
  `id` int(11) NOT NULL,
  `id_user_order` int(11) NOT NULL,
  `waktu_servis` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `catatan_tambahan` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user_order_detail`
--

INSERT INTO `user_order_detail` (`id`, `id_user_order`, `waktu_servis`, `status`, `catatan_tambahan`) VALUES
(1, 1, 113253, 1, 'Tidak ada catatan tambahan'),
(2, 2, 113242, 2, 'tidak ada catatan tambahan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `main_kendaraan_tipe`
--
ALTER TABLE `main_kendaraan_tipe`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `main_servis`
--
ALTER TABLE `main_servis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `main_sub_servis`
--
ALTER TABLE `main_sub_servis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `main_sub_servis_paket`
--
ALTER TABLE `main_sub_servis_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `main_sub_servis_paket_harga`
--
ALTER TABLE `main_sub_servis_paket_harga`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mitra`
--
ALTER TABLE `mitra`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mitra_owner`
--
ALTER TABLE `mitra_owner`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mitra_service`
--
ALTER TABLE `mitra_service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mitra_sub_service`
--
ALTER TABLE `mitra_sub_service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mitra_viewer`
--
ALTER TABLE `mitra_viewer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_alamat`
--
ALTER TABLE `user_alamat`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_kendaraan`
--
ALTER TABLE `user_kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_order`
--
ALTER TABLE `user_order`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_order_detail`
--
ALTER TABLE `user_order_detail`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `main_kendaraan_tipe`
--
ALTER TABLE `main_kendaraan_tipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `main_servis`
--
ALTER TABLE `main_servis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `main_sub_servis`
--
ALTER TABLE `main_sub_servis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `main_sub_servis_paket`
--
ALTER TABLE `main_sub_servis_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `main_sub_servis_paket_harga`
--
ALTER TABLE `main_sub_servis_paket_harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `mitra`
--
ALTER TABLE `mitra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `mitra_owner`
--
ALTER TABLE `mitra_owner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `mitra_service`
--
ALTER TABLE `mitra_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mitra_sub_service`
--
ALTER TABLE `mitra_sub_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mitra_viewer`
--
ALTER TABLE `mitra_viewer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user_alamat`
--
ALTER TABLE `user_alamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `user_kendaraan`
--
ALTER TABLE `user_kendaraan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user_order`
--
ALTER TABLE `user_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_order_detail`
--
ALTER TABLE `user_order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
