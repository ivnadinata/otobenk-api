<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MitraModel extends CI_Model {

	public function GetMitraOwner($id_user='')
	{
		return $this->db->get_where('mitra_owner', ['id_user'=>$id_user])->row_array();
	}

	
	public function MitraBengkelUpdate($id_mitra_owner='', $data)
	{
		
		return $this->db->update('mitra', $data, ['id_mitra_owner'=>$id_mitra_owner]);

	}

	public function GetMitraBengkel($user_bengkel='')
	{
		
		return $this->db->get_where('mitra', ['user_bengkel'=>$user_bengkel])->row_array();

	}

	public function GetMitraBengkelByGps($lat='', $lon='')
	{
		$sql = "SELECT * FROM (
				    SELECT *, 
				        (
				            (
				                (
				                    acos(
				                        sin(( ? * pi() / 180))
				                        *
				                        sin(( `lat` * pi() / 180)) + cos(( ? * pi() /180 ))
				                        *
				                        cos(( `lat` * pi() / 180)) * cos((( ? - `lon`) * pi()/180)))
				                ) * 180/pi()
				            ) * 60 * 1.1515 * 1.609344
				        )
				    as distance FROM `mitra`
				) myTable
				WHERE distance <= 10
				ORDER BY distance ASC";

		return $this->db->query($sql, array($lat, $lat, $lon));

	}

	public function GetListOrder($id_mitra='')
	{
		$sql = "SELECT user_order.id, main_servis.nama_servis, main_sub_servis.id as id_main_sub_servis, main_sub_servis.nama_sub_servis, main_kendaraan_tipe.id as id_main_kendaraan_tipe, main_kendaraan_tipe.nama_tipe, user_detail.id_user, user_detail.nama_lengkap, user_order_detail.waktu_servis, user_order_detail.status, user_order_detail.catatan_tambahan FROM 
		user_order
		LEFT JOIN main_sub_servis ON main_sub_servis.id = user_order.id_main_sub_servis
		LEFT JOIN user_detail ON user_detail.id_user = user_order.id_user
		LEFT JOIN user_order_detail ON user_order_detail.id_user_order = user_order.id
		LEFT JOIn main_kendaraan_tipe ON main_kendaraan_tipe.id = user_order.id_main_kendaraan_tipe
		LEFT JOIN main_servis ON main_servis.id = user_order.id_main_servis
		WHERE user_order.id_mitra = ?";

		return $this->db->query($sql, array($id_mitra))->result();
		
	}

	public function GetDetailOrder($id_mitra='', $id_order='')
	{
		$sql = "SELECT user_order.id, main_servis.nama_servis, main_sub_servis.id as id_main_sub_servis, main_sub_servis.nama_sub_servis, main_kendaraan_tipe.id as id_main_kendaraan_tipe, main_kendaraan_tipe.nama_tipe, user_detail.id_user, user_detail.nama_lengkap, user_order_detail.waktu_servis, user_order_detail.status, user_order_detail.catatan_tambahan FROM 
		user_order
		LEFT JOIN main_sub_servis ON main_sub_servis.id = user_order.id_main_sub_servis
		LEFT JOIN user_detail ON user_detail.id_user = user_order.id_user
		LEFT JOIN user_order_detail ON user_order_detail.id_user_order = user_order.id
		LEFT JOIn main_kendaraan_tipe ON main_kendaraan_tipe.id = user_order.id_main_kendaraan_tipe
		LEFT JOIN main_servis ON main_servis.id = user_order.id_main_servis
		WHERE user_order.id_mitra = ? AND user_order.id = ?";

		return $this->db->query($sql, array($id_mitra, $id_order))->row_array();
	}


}

/* End of file MitraModel.php */
/* Location: ./application/models/MitraModel.php */
