<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model {

	public function UserCheck($email=null)
	{
		return $this->db->get_where('user', ['email'=>$email])->num_rows();
	}

	public function UserInsert($data, $level)
	{
		
		if ($level==1) {
			$this->db->insert('user', $data['user']);
			$id_user = $data['user']['id'];  //$this->db->insert_id();
			$this->db->insert('user_detail', ['id_user'=>$id_user, 'nama_lengkap'=>$data['user_detail']['nama_lengkap'] ]);
		} else {
			$this->db->insert('user', $data['user']);
			$id_user = $data['user']['id'];  //$this->db->insert_id();
			$this->db->insert('mitra_owner', ['id_user'=>$id_user, 'nama_lengkap'=>$data['user_detail']['nama_lengkap'] ]);
			$id_owner = $this->db->insert_id();
			$this->db->insert('mitra', ['id_mitra_owner'=>$id_owner]);
		}

	}

	public function UserLog($email = '')
	{
		return $this->db->get_where('user', ['email'=>$email])->row_array();
	}

	public function UserDetail($id='')
	{
		$user = $this->db->get_where('user', ['id'=>$id])->row_array();
		$id_user = $user['id'];

		if ($user['level']==1) {
			$sql ="SELECT user.id, user.email, user.level, user.verified, user.date_created, user_detail.nama_lengkap, user_detail.jk, user_detail.no_wa, user_detail.no_hp, 
			CONCAT(IF(kendaraan_merek.jenis=2,'Motor','Mobil'), ' ', kendaraan_merek.merek, ' ', kendaraan_merek.nama) AS kendaraan, CONCAT('true') AS status  
            FROM user_detail 
			LEFT JOIN user ON user_detail.id_user = user.id
			LEFT JOIN user_kendaraan ON user_kendaraan.id_user = user_detail.id_user
			LEFT JOIN kendaraan_merek ON kendaraan_merek.id = user_kendaraan.id_kendaraan_merek WHERE user.id = ?";
			return $this->db->query($sql, array($id_user))->row_array();	

		} elseif ($user['level']==2) {
			$sql = "SELECT user.id, user.email, mitra_owner.nama_lengkap, mitra.nama_bengkel, mitra.foto_bengkel, user.level, user.date_created, user.verified, mitra_owner.nama_lengkap, mitra_owner.jk, mitra_owner.no_hp, mitra_owner.no_wa FROM user 
			LEFT JOIN mitra_owner ON mitra_owner.id_user = user.id
			LEFT JOIN mitra ON mitra.id_mitra_owner = mitra_owner.id
			WHERE user.id = ?";
			return $this->db->query($sql, array($id_user))->row_array();

		}

	}

	public function UserProfileUpdate($id_user='',$level='', $data)
	{
		
		if ($level=='1') {
			
			return $this->db->update('user_detail', $data, ['id_user'=>$id_user]);

		} elseif ($level=='2') {
			
			return $this->db->update('mitra_owner', $data, ['id_user'=>$id_user]);

		}

	}

	public function GetPaketServis($id_sub_servis='', $id_kendaraan='')
	{
		$sql = "SELECT main_servis.nama_servis, main_sub_servis.nama_sub_servis, main_kendaraan_tipe.nama_tipe, main_sub_servis_paket.nama_item_servis, main_sub_servis_paket_harga.harga FROM main_sub_servis_paket 
		LEFT JOIN main_sub_servis_paket_harga ON main_sub_servis_paket_harga.id_main_sub_servis_paket = main_sub_servis_paket.id
		LEFT JOIN main_sub_servis ON main_sub_servis.id = main_sub_servis_paket.id_main_sub_servis
		LEFT JOIN main_kendaraan_tipe ON main_kendaraan_tipe.id = main_sub_servis_paket_harga.id_main_kendaraan_tipe
		LEFT JOIN main_servis ON main_servis.id = main_sub_servis.id_main_servis
		WHERE main_sub_servis.id = ? AND main_kendaraan_tipe.id = ? AND main_servis.id = 1";

		return $this->db->query($sql, array($id_sub_servis, $id_kendaraan));
	}

	public function GetMitraBengkelByGps($lat='', $lon='')
	{
		$sql = "SELECT * FROM (
				    SELECT *, 
				        (
				            (
				                (
				                    acos(
				                        sin(( ? * pi() / 180))
				                        *
				                        sin(( `lat` * pi() / 180)) + cos(( ? * pi() /180 ))
				                        *
				                        cos(( `lat` * pi() / 180)) * cos((( ? - `lon`) * pi()/180)))
				                ) * 180/pi()
				            ) * 60 * 1.1515 * 1.609344
				        )
				    as distance FROM `mitra`
				) myTable
				WHERE distance <= 10
				ORDER BY distance ASC limit 1";

		return $this->db->query($sql, array($lat, $lat, $lon))->row_array();

	}

	public function GetListOrder($id_user='')
	{
		$sql = "SELECT user_order.id, main_servis.nama_servis, main_sub_servis.id as id_main_sub_servis, main_sub_servis.nama_sub_servis, main_kendaraan_tipe.id as id_main_kendaraan_tipe, main_kendaraan_tipe.nama_tipe, mitra.nama_bengkel, user_order_detail.waktu_servis, user_order_detail.status, user_order_detail.catatan_tambahan FROM 
		user_order
		LEFT JOIN main_sub_servis ON main_sub_servis.id =  user_order.id_main_sub_servis
		LEFT Join mitra ON mitra.id = user_order.id_mitra
		LEFT JOIN user_order_detail ON user_order_detail.id_user_order = user_order.id
		LEFT JOIn main_kendaraan_tipe ON main_kendaraan_tipe.id = user_order.id_main_kendaraan_tipe
		LEFT JOIN main_servis ON main_servis.id = user_order.id_main_servis
		WHERE user_order.id_user = ?";

		return $this->db->query($sql, array($id_user))->result();
		
	}

	public function GetOrderDetail($id_user, $id_order)
	{
		$sql = "SELECT user_order.id, main_servis.nama_servis, main_sub_servis.id as id_main_sub_servis, main_sub_servis.nama_sub_servis, main_kendaraan_tipe.id as id_main_kendaraan_tipe, main_kendaraan_tipe.nama_tipe, mitra.nama_bengkel, user_order_detail.waktu_servis, user_order_detail.status, user_order_detail.catatan_tambahan FROM 
		user_order
		LEFT JOIN main_sub_servis ON main_sub_servis.id =  user_order.id_main_sub_servis
		LEFT Join mitra ON mitra.id = user_order.id_mitra
		LEFT JOIN user_order_detail ON user_order_detail.id_user_order = user_order.id
		LEFT JOIn main_kendaraan_tipe ON main_kendaraan_tipe.id = user_order.id_main_kendaraan_tipe
		LEFT JOIN main_servis ON main_servis.id = user_order.id_main_servis
		WHERE user_order.id_user = ? AND user_order.id = ?";

		return $this->db->query($sql, array($id_user, $id_order))->row_array();
	}

}

/* End of file UsersModel.php */
/* Location: ./application/models/UsersModel.php */
