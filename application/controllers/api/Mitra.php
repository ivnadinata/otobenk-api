<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class Mitra extends RestController {

		public function __construct()
		{
			parent::__construct();
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Headers: apikey');
			$this->load->model('MitraModel');
		}

		public function bengkel_get()
		{
			
			$by = $this->get('get');

			if ($by=='all') {
				


			} elseif ($by=='gps') {
				
				$lat = $this->get('lat');
				$lon = $this->get('lon');

				if (empty($lat) && empty($lon)) {
					$this->response(['status'=>false, 'message'=>'0'], 403);
				} else {

					$data_bengkel = $this->MitraModel->GetMitraBengkelByGps($lat, $lon);
					$data = array();
					
					foreach ($data_bengkel->result_array() as $row) {

						if (round($row['distance'],2)*1000 > 1000) {

							$data[] = [
								'id'=>$row['id'],
								'id_mitra_owner'=>$row['id_mitra_owner'],
								'user_bengkel'=>$row['user_bengkel'],
								'nama_bengkel'=>$row['nama_bengkel'],
								'alamat'=>$row['alamat'],
								'tipe'=>$row['tipe'],
								'lat'=>$row['lat'],
								'lon'=>$row['lon'],
								'foto_bengkel'=>$row['foto_bengkel'],
								'jam_buka'=>$row['jam_buka'],
								'jam_tutup'=>$row['jam_tutup'],
								'jarak'=> round($row['distance'],1),
								'satuan'=>"Kilo Meter"
							];

						} else {

							$data[] = [
								'id'=>$row['id'],
								'id_mitra_owner'=>$row['id_mitra_owner'],
								'user_bengkel'=>$row['user_bengkel'],
								'nama_bengkel'=>$row['nama_bengkel'],
								'alamat'=>$row['alamat'],
								'tipe'=>$row['tipe'],
								'lat'=>$row['lat'],
								'lon'=>$row['lon'],
								'foto_bengkel'=>$row['foto_bengkel'],
								'jam_buka'=>$row['jam_buka'],
								'jam_tutup'=>$row['jam_tutup'],
								'jarak'=> round($row['distance'],2)*1000,
								'satuan'=>"Meter"
							];

						}

					}

					$this->response(['status'=>true, 'data'=>$data],200);

				}

			} elseif ($by=='username') {
				


			}

		}


	/**
		*Profile Bengkel Mitra API
		*-----------------
		*@method : PUT
		*@link   : api/mitra/bengkel
		*-----------------
		*@param id_user
		*@param nama_bengkel
		*@param user_bengkel
		*@param alamat
		*@param tipe
		*@param lat
		*@param lon
		*@param foto_bengkel
		*@param jam_buka
		*@param jam_tutup
	*/

		public function bengkel_put()
		{
			
			$id_user = $this->put('id_user');
			$data_mitra = $this->MitraModel->GetMitraOwner($id_user);
			$data = [
				'nama_bengkel' => $this->put('nama_bengkel'),
				'user_bengkel' => $this->put('user_bengkel'),
				'alamat' => $this->put('alamat'),
				'tipe' => $this->put('tipe'),
				'lat' => $this->put('lat'),
				'lon' => $this->put('lon'),
				'foto_bengkel' => $this->put('foto_bengkel'),
				'jam_buka' => $this->put('jam_buka'),
				'jam_tutup' => $this->put('jam_tutup')
			];
			
			if (empty($data_mitra)) {

				$this->response(['status'=>false, 'message'=>'user tidak ada']);

			} else {

				$update = $this->MitraModel->MitraBengkelUpdate($data_mitra['id'], $data);

				if (!empty($update)) {
					
					$this->response(['status'=>true, 'message'=>'data telah diubah']);

				} else {
					$this->response(['status'=>false, 'message'=>'terjadi kesalahan']);
				}

			}

		}

		/**
		*List Order Mitra API
		*-----------------
		*@method : GET
		*@link   : api/mitra/listorder
		*-----------------
		*@param id_mitra
		*/
		public function listorder_get()
		{
			$id_mitra = $this->get('id_mitra');

			$data = $this->MitraModel->GetListOrder($id_mitra);

			if (!empty($data)) {
				$this->response($data, 200);
			} else {
				$this->response(['status'=>false, 'message'=>'belum ada pesan'],404);
			}

		}

		/**
		*Detail Order Mitra API
		*-----------------
		*@method : GET
		*@link   : api/mitra/listorder
		*-----------------
		*@param id_mitra
		*@param id_order
		*/
		public function detailorder_get()
		{

			$this->load->model('UsersModel');

			$id_mitra = $this->get('id_mitra');
			$id_order = $this->get('id_order');

			$data = $this->MitraModel->GetDetailOrder($id_mitra, $id_order);
			$order_detail = $this->UsersModel->GetPaketServis($data['id_main_kendaraan_tipe'], $data['id_main_sub_servis'])->result();

			$total_sum = 0;
			foreach ($order_detail as $key) {
				//echo $key->harga."\n";
				$total_sum += $key->harga;

			}

			if (!empty($data)) {
				$this->response(['order'=>$data, 'paket_order'=>$order_detail, 'total_bayar'=>$total_sum], 200);
			} else {
				$this->response(['status'=>false, 'message'=>'belum ada pesan'],404);
			}

		}

		

		public function main_service_post()
		{
			
			$id_user = $this->post('id_user');
			$data_mitra = $this->MitraModel->GetMitraOwner($id_user);
			$id_owner = $data_mitra['id'];

		}

}

/* End of file Mitra.php */
/* Location: ./application/controllers/api/Mitra.php */
