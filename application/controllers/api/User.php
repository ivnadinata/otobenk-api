<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use chriskacerguis\RestServer\RestController;

class User extends RestController {


		public function __construct()
		{
			parent::__construct();
			header('Access-Control-Allow-Origin: *');
			header('Access-Control-Allow-Headers: apikey,content-type');
			$this->load->model('UsersModel');
		}


	/**
		*Registrasi User API
		*-----------------
		*@method : POST
		*@link   : api/user/registrasi
		*-----------------
		*@param email
		*@param password
		*@param level
		*@param nama_lengkap
	*/
		public function registrasi_post()
		{

			$kode_verified = substr(md5(microtime()), 0,5);
			$genID = md5(time()*2);

			$status = null;
			$data = [
				'user' => [
					'id' => $genID,
					'email' => $this->post('email', true),
					'password' => password_hash($this->post('password', true), PASSWORD_DEFAULT),
					'level' => $this->post('level', true),
					'date_created' => time(),
					'verified'=>0,
					'kode_verified'=> $kode_verified
				],
				'user_detail' => [
					'nama_lengkap' => $this->post('nama_lengkap', true)
				]
			];

			foreach ($data as $key => $value) {
				foreach ($value as $keys => $result) {
					if ($result===null) {
						$status = 0;
					} else {
						$status = 1;
					}
				}
			}


			if ($status===1) {
				
				$cek = $this->UsersModel->UserCheck($data['user']['email']);

				if ($cek===0) {

					$this->UsersModel->UserInsert($data, $data['user']['level']);

					$this->response(['status'=>true, 'message'=>'registrasi succes'], 201);

				} else {
					$this->response(['status'=>false, 'message'=>'email exist'], 403);
				}

			} else {
				$this->response(['status'=>false, 'message'=>'kolom wajib di isi'], 403);
			}

		}


	/**
		*Login User API
		*-----------------
		*@method : POST
		*@link   : api/user/login
		*-----------------
		*@param email
		*@param password
	*/
		public function login_post()
		{

			$email    = $this->post('email', TRUE);
			$password = $this->post('password', TRUE);
			$user 	  = $this->UsersModel->UserLog($email);

			if (!empty($email) && !empty($password)) {
				if ($user) {
					
					if (password_verify($password, $user['password'])) {

						$status_verified;

						if ($user['verified']==1) {
							$status_verified = true;
						} else {
							$status_verified = false;
						}
						
						$this->response([
							'status'=>true, 
							'data'=>[
								'id_user'=>$user['id'],
								'email'=>$user['email'],
								'level'=>$user['level'],
								'verified'=>$status_verified
							]
						], 200);

					} else {

						$this->response([
							'status'=>false, 
							'code' => 51,
							'message'=>'email atau password salah'], 403);

					}

				} else {
					$this->response([
						'status'=>false,
						'code' => 52,
						'message'=>'email tidak ada'], 403);
				}
			} else {
				$this->response(['status'=>false, 'message'=>'kolom wajib diisi'], 403);
			}

		}


	/**
		*Profil User API
		*-----------------
		*@method : GET
		*@link   : api/user/profil
		*-----------------
		*@param ID_USER
	*/
		public function profile_get()
		{

			$id_user = $this->get('id_user');
			$data = $this->UsersModel->UserDetail($id_user);
			
			if (!empty($data)) {
				$this->response($data, 200);
			} else {
				$this->response(['status'=>false, 'message'=>'user tidak ada'], 404);
			}

		}

	/**
		*Servis User API
		*-----------------
		*@method : GET
		*@link   : api/user/paketservis
		*-----------------
		*@param id_kendaraan
		*@param id_sub_servis
	*/
		public function paketservis_get()
		{
			$id_kendaraan = $this->get('id_kendaraan');
			$id_sub_servis = $this->get('id_sub_servis');

			$data = $this->UsersModel->GetPaketServis($id_sub_servis, $id_kendaraan);
			
			if (!empty($data->result_array())) {
				$this->response($data->result_array(),200);
			} else {
				$this->response(['status'=>false, 'message'=>'tidak ada paket servis'],404);
			}
			
		}

	/**
		*Servis User API
		*-----------------
		*@method : GET
		*@link   : api/user/listorder
		*-----------------
		*@param id_user
	*/
		public function listorder_get()
		{
			$id_user = $this->get('id_user');

			$data = $this->UsersModel->GetListOrder($id_user);

			if (!empty($data)) {
				$this->response($data, 200);
			} else {
				$this->response(['status'=>false, 'message'=>'belum ada pesan'],404);
			}

		}

	/**
		*Servis User API
		*-----------------
		*@method : GET
		*@link   : api/user/orderdetail
		*-----------------
		*@param id_user
		*@param id_order
	*/
		public function orderdetail_get()
		{
			$id_user = $this->get('id_user');
			$id_order = $this->get('id_order');

			$data = $this->UsersModel->GetOrderDetail($id_user, $id_order);
			$order_detail = $this->UsersModel->GetPaketServis($data['id_main_kendaraan_tipe'], $data['id_main_sub_servis'])->result();

			$total_sum = 0;
			foreach ($order_detail as $key) {
				//echo $key->harga."\n";
				$total_sum += $key->harga;

			}

			if (!empty($data)) {
				$this->response(['order'=>$data, 'paket_order'=>$order_detail, 'total_bayar'=>$total_sum], 200);
			} else {
				$this->response(['status'=>false, 'message'=>'belum ada pesan'],404);
			}

		}

	/**
		*Servis User API
		*-----------------
		*@method : GET
		*@link   : api/user/bengkelrecomend/
		*-----------------
		*@param lat
		*@param lon
	*/
		public function bengkelrecomend_get()
		{
			$lat = $this->get('lat');
			$lon = $this->get('lon');

			if (empty($lat) && empty($lon)) {
				$this->response(['status'=>false, 'message'=>'0'], 403);
			} else {

				$data_bengkel = $this->UsersModel->GetMitraBengkelByGps($lat, $lon);
				$data = array();

				if (round($data_bengkel['distance'],2)*1000 > 1000) {

					$data[] = [
						'id'=>$data_bengkel['id'],
						'id_mitra_owner'=>$data_bengkel['id_mitra_owner'],
						'user_bengkel'=>$data_bengkel['user_bengkel'],
						'nama_bengkel'=>$data_bengkel['nama_bengkel'],
						'alamat'=>$data_bengkel['alamat'],
						'tipe'=>$data_bengkel['tipe'],
						'lat'=>$data_bengkel['lat'],
						'lon'=>$data_bengkel['lon'],
						'foto_bengkel'=>$data_bengkel['foto_bengkel'],
						'jam_buka'=>$data_bengkel['jam_buka'],
						'jam_tutup'=>$data_bengkel['jam_tutup'],
						'jarak'=> round($data_bengkel['distance'],1),
						'satuan'=>"Kilo Meter"
					];

				} else {

					$data[] = [
						'id'=>$data_bengkel['id'],
						'id_mitra_owner'=>$data_bengkel['id_mitra_owner'],
						'user_bengkel'=>$data_bengkel['user_bengkel'],
						'nama_bengkel'=>$data_bengkel['nama_bengkel'],
						'alamat'=>$data_bengkel['alamat'],
						'tipe'=>$data_bengkel['tipe'],
						'lat'=>$data_bengkel['lat'],
						'lon'=>$data_bengkel['lon'],
						'foto_bengkel'=>$data_bengkel['foto_bengkel'],
						'jam_buka'=>$data_bengkel['jam_buka'],
						'jam_tutup'=>$data_bengkel['jam_tutup'],
						'jarak'=> round($data_bengkel['distance'],2)*1000,
						'satuan'=>"Meter"
					];

				}

				$this->response(['status'=>true, 'data'=>$data],200);

			}



		}


	/**
		*Profil User API
		*-----------------
		*@method : PUT
		*@link   : api/user/profil
		*-----------------
		*@param id_user
		*@param nama_lengkap
		*@param jk
		*@param no_wa
		*@param no_hp

	*/	

		public function profile_put()
		{
			$id_user  = $this->put('id_user');
			$data 	  = $this->UsersModel->UserDetail($id_user);
			$data_put = [
							'nama_lengkap'=>$this->put('nama_lengkap'),
							'jk'=>$this->put('jk'),
							'no_wa'=>$this->put('no_wa'),
							'no_hp'=>$this->put('no_hp')
						];

			if (!empty($data)) {
				
				$update = $this->UsersModel->UserProfileUpdate($id_user, $data['level'], $data_put);

				if ($update!==null) {
					
					$this->response(['status'=>true, 'message'=>'data '.$data['nama_lengkap'].' berhasil diubah'], 200);

				} else {
					$this->response(['status'=>false, 'message'=>'gagal'], 502);
				}

			} else {
				$this->response(['status'=>false, 'message'=>'user tidak ada'], 404);
			}

		}

		public function emailcheck_get()
		{
			$email = $this->get('email');
			if (!empty($email)) {
				
				$cek = $this->UsersModel->UserCheck($email);

				if ($cek==1) {
					$this->response(['status'=>false, 'data'=>$cek],404);
				} else {
					$this->response(['status'=>false, 'data'=>$cek],200);
				}

				

			}
		}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
